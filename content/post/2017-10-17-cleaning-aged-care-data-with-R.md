---
title: Cleaning and exploring aged care data in R
date: 2017-10-17
tags: ['R', 'cleaning','exploration','aged care']
---

I've cleaned and performed some basic exploration (in R) of the [Aged Care Services List](https://agedcare.health.gov.au/ageing-and-aged-care-overview/about-aged-care/aged-care-service-providers-in-australia). The Australian Government Department of Health releases the Aged Care Services List every year (from 2009). This list contains information on aged care services subsidised by the Australian Government such as:

* name of the organisation providing care (where care can be residential or home-based care);
* the number of beds in a residential care facility;
* the number of home care packages held by a provider;
* the number of transitional care places;
* the government funding allocated (if known); and
* various geographical classifications, e.g. statistical local area and remoteness classification.

I've looked at the Aged Care Services List from 2009 - 2016 and extracted information about home care and residential care for each year. See my code [here](https://gitlab.com/She-Ra/Exploring-Aged-Care-data-in-Australia/tree/master) to see how I cleaned and explored the data below.

# Home care
Here home care refers to the [Home Care Packages (HCP) Programme](https://agedcare.health.gov.au/programs/home-care/about-the-home-care-packages-program), which is basically an Australian Government subsidy paid to an approved aged care provider, who then coordinates a bunch of services to help care for older Australians. HCPs help older Australians stay at home by providing personal care and clinical care services (e.g. showering, cleaning, visits from a nurse etc.). HCPs range from level 1 (low care) to level 4 (high care), with the subsidy amount increasing with increasing care level.

Before 2015, aged care packages and the associated block of funding were assigned to providers. If an individual required a HCP, they would be placed on a national waiting list. When a HCP became available the individual would be assigned a HCP and a provider. Individuals could not chose their provider and had little say in how their care was delivered; indeed with subsidies paid by the government there was little incentive for providers to tailor their services to the individual.  

In 2015 it became mandatory for HCP to be delivered according to the principles of [Consumer Directed Care (CDC)](https://agedcare.health.gov.au/aged-care-reform/home-care/home-care-packages-reform), meaning individuals could decide what services they received and by whom. Practically this means HCP funding is allocated to an individual who then choses a provider to work with them to service their needs. Providers now must compete with each other to attract and simply hold onto individuals with HCPs.

### How have providers responded to changes in the delivery of home care?
The table below shows providers consolidated heavily in 2014 in preparation for the CDC changes coming in 2015. In 2014 the number of providers decreased substantially from 515 to only 81, with the average number of packages held by a single provider increasing by 80% to 50 packages. Interestingly the average funding per package dropped by almost half from 2014 to 2012 (no funding information was available in 2013), though the overall funding increased. This requires more investigation, in particular the table below includes data for providers who only provide home care, not providers who provide home care and residential/transitional care, meaning 483 service providers were excluded from the data below out of a total of 11,677 service providers.

| year | average packages per provider | average funding per provider [$] | average funding per package [$] | unique providers |
|:----:|:-----------------------------:|:--------------------------------:|:-------------------------------:|:----------------:|
| 2009 |         30                    | 468,749                          | 15,668                          | 490              |
| 2010 |         29                    | 463,038                          | 16,070                          | 499              |
| 2011 |         26                    | 436,335                          | 16,978                          | 500              |
| 2012 |         25                    | 447,579                          | 17,820                          | 507              |
| 2013 |         28                    | NaN                              | NaN                             | 515              |
| 2014 |         51                    | 501,884                          | 9,797                           | 81               |
| 2015 |         51                    | 618,115                          | 12,207                          | 164              |
| 2016 |         61                    | 936,733                          | 15,289                          | 198              |

The above summary was achieved by looking at data from 11,194 different service providers.

### How are home care packages distributed?
This histogram below can be used to comment on:

* the total number of HCPs in the market, and
* how those HCPs were distributed among service providers.

Clearly the total number of HCPs dropped from 2013 to 2014, indeed the total number of packages in 2013 was 60,520 and in 2014 this had been reduced to 8,043. In 2016 the total number of HCPs subsidised by the government was 33,820. This drop is worth looking into further - was this drop related to a change in reporting requirements? Was the drop related to changes in funding (or the application process)?

As for the distribution of HCPs, from 2009 - 2013 the majority of service providers held less than 50 packages, indeed a great number serviced less than 15-20 packages. After 2015 the histogram starts to look like a hill rather than a slide, meaning providers tend to service a greater number of packages.

![Histogram showing the number of HCP serviced by providers](images/2017-10-17_hist_packages.png)

### How has funding changed per package from 2009 - 2016?
The plots below depict government funding as a function of HCPs. From 2009 - 2012 there are two distinct branches, suggesting a high and low care funding mechanism, with providers focusing on one or the other. From 2014 onwards level 1 to level 4 HCPs are not reported separately, thus one data point could represent HCPs of varying levels.

As an aside the number of points decreased in 2013 suggesting a dip in the total number of packages.

![Plot of HCPs versus government funding.](images/2017-10-17_hc_funds.png)

{{< figure src="images/2017-10-17_hc_funds.png" title="Plot of HCPs versus government funding.">}}

# Residential care
Residential aged care is provided in a specialised facility on a permanent or short-term basis, and includes accomodation, personal care, clinical care and other services (e.g. laundry). Residential aged care is funded by the Australian Government and by contributions from residents. Tabulated below are summary data on government funding, beds and providers from 2009 to 2016.

The table below shows the number of providers reduced heavily in 2013, from 1086 providers in 2012 down to 528. Perhaps providers consolidated, which explains why the average number of beds per provider increased by almost 40%. After 2014 the number of providers increases and hovers back around the 1000 mark, with the number of beds reducing from around ninety-something down to sixty-something, suggesting deconsolidation.

Those in the aged care industry I've spoken too note people are entering residential care with more complex needs today than in the past, which may explain why the average funding per bed has increased year on year.

| year | average beds per provider | average funding per provider [$1M] | average funding per bed [$1K] | unique providers   |
|:----:|:-------------------------:|:----------------------------------:|:-----------------------------:|:------------------:|
| 2009 |     62.06889              | 2.269821                           | 36.56939                      | 1162               |
| 2010 |     63.88254              | 2.513044                           | 39.33852                      | 1147               |
| 2011 |     64.75850              | 2.814041                           | 43.45439                      | 1129               |
| 2012 |     66.68665              | 3.138914                           | 47.06961                      | 1086               |
| 2013 |     91.16355              | NaN                                | NaN                           | 528                |
| 2014 |     86.70962              | 4.437138                           | 51.17238                      | 563                |
| 2015 |     68.11018              | 3.731928                           | 54.79251                      | 1013               |
| 2016 |     69.32534              | 4.009693                           | 57.83877                      | 989                |

### Distribution of aged care beds
The histogram below shows the distribution of the number of beds, which until 2012 is relatively stable. Surprisingly there is a dip in 2013 - 2014 indicating a drop in beds (from 185,789 beds in 2012 to 92,531 beds in 2013). As beds would be hard to add or remove (requiring extra wings to be built or destroyed in care facilities) I assume the change in 2013 and 2014 relates to funding - perhaps some providers lost government funding and thus were not part of these reporting rounds? Perhaps reporting changed?

In 2015 - 2016 the funding seemed to recover, with 199,449 beds (and associated providers) receiving funding in 2015.

![Histogram showing the number of beds held by providers](images/2017-10-17_hist_resi.png)

{{< figure src="images/2017-10-17_hist_resi.png" title="Histogram showing the number of beds held by providers">}}

### Funding residential aged care from 2009 - 2016
Government funding as a function of the number of beds is shown below. Even by eye it's clear the slope of the data is increasing, meaning aged care is getting more expensive year on year - does the increase in this slope match inflation? Or perhaps, as I've heard people are coming to residential aged care facilities in a frailer state, therefore care is much more costly.

![Plot of beds versus funding](images/2017-10-17_fund_resi.png)

{{< figure src="2017-10-17_fund_resi.png" title="Plot of beds versus funding">}}


