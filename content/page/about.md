---
title: About me
subtitle: Data Science | Python | R | SQL
comments: false
---

I'm Mac, I like R and Python. I have a PhD in physics, an undergrad in pure maths and I'm ready to jump on the data science bandwagon! 

This blog documents my progress as I work through R / Python courses and projects.

Please check out my projects!


