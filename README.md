![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

These are my notes to help me maintain my website.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Editing menus, titles, front matter etc.](#editing)
- [Checking posts from the command line](#checking-posts)
- [Generating new posts](#new-posts)
- [Removing posts](#removing-posts)
- [Useful links](#useful-links)
- [Things I still need to do](#to-do-list)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Editing

To edit main menus (top of page), title, links (bottom of page) etc.
1. Go to project home,
2. open file _config.toml_,
3. edit file and save (editing is fairly straight forward).
4. git add / commit / push / pull as required.

### Front matter
Front matter is the header of a blogpost, it looks like this:
```
---
title: ""
subtitle: ""
date:
tags: []
---
```
To edit front matter:
1. navigate to the file `default.md` found here: `home themes/beautifulhugo/archetypes/default.md` 
2. insert front matter as required (see a list [here](https://gohugo.io/content-management/front-matter/#front-matter-variables))
3. save changes
4. git add / commit / push / pull as required.

Be careful of any typos, also if posts are to upload successfully a title and date are required otherwise "Failed" will appear on the Pipelines report.

I've configured my website to update automatically. Check Pipelines for progress.

## Checking posts

1. Open command line and navigate into Hugo folder ("home")
2. type `Hugo` for a list of content.

## New posts

This adds a live post

1. Open command line and navigate into Hugo folder (same folder with config file)
2. generate a new post: `hugo new post/my-post-name.md`
3. open* the new file `my-post-name.md`.
4. insert title, date and other fields
5. below title add a new line for the date**: `date: YYYY-MM-DD` (posts order automatically by date)
6. save file
7. git add / commit / push / pull as required.

\* you can open and edit the file in GitLabs (need to push to GitLabs first) or use the command line interface (CLI) and Vim (see below).

### Draft posts
All posts have front matter as shown below. If `draft: true` the post will not be published. If `draft: false` the post will be published. 

```
---
title: ""
subtitle: ""
date: 2017-01-29
draft: true
tags: []
---
```

### Editing post content from CLI using vim

1. In the command line, navigate to post location `content/post/post-name.md`
2. type `vim post-name.md`
3. type `i` for insert mode (see more vim commands [here](http://vimsheet.com/))
4. insert required text
5. press esc to exit insert mode, type `:wq` to save and quit
6. git add / commit / push / pull as required.

I've configured my website to update automatically. Check Pipelines for progress.

## Removing posts

To perminantly delete a post, as far as I can tell, you just delete the `.md` file for the post directly

To keep posts which do not publish to the website
1. navigate to your post (`.md` file) 
2. add the line `expiryDate: YYYY-MM-DD` into the front matter (see example below)
3. save file
4. git add / commit / push / pull as required.

```
---
title: ""
subtitle: ""
date: 2017-01-29
expiryDate: YYYY-MM-DD
draft: true
tags: []
---
```

## Useful links
* Getting started with Hugo: https://gohugo.io/getting-started/
* For more CLI commands: https://gohugo.io/getting-started/usage/
* The theme used is adapted from http://themes.gohugo.io/beautifulhugo/
* Learn more about GitLab Pages at https://pages.gitlab.io
* Official documentation for gitlab pages https://docs.gitlab.com/ce/user/project/pages/
 
## To do list

my to do list

* Fix photogallery type post
* Fix big image gallery type post
* How to convert/move a markdown file from gitlabs to website.